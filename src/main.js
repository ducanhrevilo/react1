import React from "react";
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import Rank from "./rankings"
export default function Page() {
	if (document.cookie) {
	  return (
		<Router>
		  <div className="header">
			<h1>Happy Data</h1>
		  </div>
		  <div id="menu" className="topnav">
			<Link to="/">Home</Link>
			<Link to="/rankings">rankings</Link>

		  </div>
  
		  <Route exact path="/" component={Home} />
		  <Route path="/rankings" component={rankingsPage} />
		</Router>
	  )
	} else {
	  return (
		<Router>
		  <div className="header">
			<h1>Happy Data Ranking</h1>
		  </div>
		  <div id="menu" className="topnav">
			<Link to="/">Home</Link>
			<Link to="/rankings">rankings</Link>
		  </div>
  
		  <Route exact path="/" component={Home} />
		  <Route path="/rankings" component={rankingsPage} />

		</Router>
	  )
	}
  }
  
  function Home() {
	return (
	  <div className="home-page">
		<h1>Welcome to Crime Database</h1>
		<p>You can find any offences here</p>
	  </div>
	)
  }
  function rankingsPage() {
	return (
	  <div>
		<h2>List of ranking</h2>
		<Rank/>
	  </div>
	);
  }
	