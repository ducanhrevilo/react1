import React, { useState } from "react";
import { baseURL } from "./api";
export default function Rank() {
  const [rankings, setRankings] = useState([]);
  var i = [];

  fetch(`${baseURL}/rankings`)
    .then(res => res.json())
    .then(res => { console.log(res);
    })
    .catch(err => { console.log(err); 
    });
  return (
  <table>
      {rankings.map(value => (
        <tr><td>{value}</td></tr>
      ))}
    </table>
  )
}