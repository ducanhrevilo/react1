import React, { useState, useEffect } from 'react';
import ReactDOM from "react-dom"

export const baseURL = "http://131.181.190.87:3000/";
function getRankings() {
    return fetch(`http://131.181.190.87:3000/rankings`)
        .then(res => res.json())
        .then(res => res.rankings)
}
export function useRankings() {
    const [offLoading, setOffLoading] = useState(true);
    const [offError, setOffError] = useState(null);
    const [rankings, setRankings] = useState([]);

    useEffect(() => {
        getRankings()
            .then(rankings => {
                setRankings(rankings);
                setOffLoading(false);
            })
            .catch(e => {
                setOffError(e);
                setOffLoading(false);
            });
    }, []);
    return {
        rankings,
        offLoading,
        offError
    };
}